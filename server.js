const Koa = require('koa')
const cors = require('@koa/cors')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const etag = require('koa-etag')
const conditional = require('koa-conditional-get')
const compress = require('koa-compress')
const puppeteer = require('puppeteer')
const exitHook = require('exit-hook')
const axios = require('axios')
const cleaner = require('clean-html')
const pkg = require('./package.json')
const randomUA = require('./fake-user-agent')

const PORT = process.env.PORT || 4001
const isDev = process.env.NODE_ENV !== 'production'

let browser
const allowMethods = ['get', 'post']

const app = new Koa()
const router = new Router()

app.use(
  compress({
    filter: function(content_type) {
      return /text/i.test(content_type)
    },
    threshold: 2048,
    flush: require('zlib').Z_SYNC_FLUSH
  })
)
app.use(conditional())
app.use(etag())

const puppeteerOptions = isDev
  ? {
      args: ['--disable-infobars'],
      executablePath: '/usr/bin/chromium-browser',
      headless: false
    }
  : {}

const cleanerOptions = {
  wrap: 0,
  'add-remove-attributes': ['style'],
  'remove-comments': true,
  'add-remove-tags': [
    'canvas',
    'form',
    'img',
    'input',
    'link',
    'noscript',
    'script',
    'style',
    'svg',
    'textarea'
  ]
}

const regSvg = /<svg[\s\S]*?>[\s\S]*?<\/svg>/g

function cleanHtml(html) {
  return new Promise(function(resolve, reject) {
    cleaner.clean(html.replace(regSvg, ''), cleanerOptions, (h) => resolve(h))
  })
}

async function runFirst() {
  browser = await puppeteer.launch(puppeteerOptions)

  const pages = await browser.pages()
  await browser.newPage()
  pages[0].close()
}

async function beforeExit() {
  await browser.close()
}

exitHook(beforeExit)

try {
  runFirst()
} catch (e) {
  console.error(e)
  process.exit(1)
}

app.use(cors())
app.use(bodyParser())

router.get('/', (ctx, next) => {
  ctx.body = `${pkg.name} - ${pkg.version}`
})

router.post('/html', async (ctx, next) => {
  const { url } = ctx.request.body
  if (!url) {
    ctx.status = 400
    return
  }

  const page = await browser.newPage()

  try {
    await page.goto(url)
  } catch (e) {
    ctx.status = 400
    return
  }

  const content = await page.content()

  ctx.body = await cleanHtml(content)

  page.close()
})

router.post('/fetch', async (ctx, next) => {
  const { url, method = 'get', clean = true, data } = ctx.request.body
  if (!url || !allowMethods.includes(method)) {
    ctx.status = 400
    return
  }

  const options = { url, method, headers: { 'User-Agent': randomUA() } }

  if (data) {
    options.data = data
  }

  const res = await axios(options)

  ctx.body = clean ? await cleanHtml(res.data) : res.data
})

app.use(router.routes()).use(router.allowedMethods())

app.listen(PORT)
